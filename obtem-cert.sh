#/bin/bash

apt update && apt install openssl -y

cat <<EOF> /certguaca/lh.conf
[req]
prompt = no
distinguished_name = req_distinguished_name

[req_distinguished_name]
C = BR
ST = SP
L = Sao Paulo
O = Guaca
OU = Acesso Remoto
emailAddress = acesso.remoto@guaca
CN = acesso.remoto
EOF

openssl req -x509 -newkey rsa:4096 -days 365 -nodes -config /certguaca/lh.conf -keyout /certguaca/selfsigned.key -out /certguaca/selfsigned.crt

rm /certguaca/lh.conf